//import { Exception } from './Exception'

//export class UserAlreadyExistsException extends Exception {
export class UserAlreadyExistsException extends Error {
  constructor () {
    super('User already exists')
  //  this.spanishMessage = 'El usuario ya existe'
  }
}
