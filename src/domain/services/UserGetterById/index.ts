import { User } from '../../entities/user/User'
import { UserRepository } from '../../repositories/UserRepository'
import { UserNotFoundException } from '../../exceptions/UserNotFoundException'


export class UserGetterById{
	private readonly _userRepository: UserRepository
	
	constructor(userRepository: UserRepository){
		this._userRepository = userRepository
	}
	
	async run(id: string): Promise<User>{
		//console.log('id xxx', id)
		const user = await this._userRepository.getById(id)
		console.log('usurio encontrado: ', user)
		if(user === null){ throw new UserNotFoundException()}		
		return user

	}
}
