//En los driving adaper van los puntos de entrada nuestros casos de uso

//import path from 'path'
//import * as dotenv from 'dotenv'
import { User } from '../../../domain/entities/user/User'
import { UserCreatorUseCase } from '../../../application/usecases/UserCreator'
import { UserGetterUseCase } from '../../../application/usecases/UserGetter'
import { InMemoryUserRepository } from '../../implementations/InMemory/InMemoryUserRepository'
//import { DynamoDBUserRepository } from '../../implementations/Aws/dynamo-db/DynamoDBUserRepository'
//import { UserGetterUseCase } from '../../../application/usecases/UserGetter'
import { UserUpdaterUseCase } from '../../../application/usecases/UserUpdater'
import { UserDeleterUseCase } from '../../../application/usecases/UserDeleter'
//import { UuidV4Generator } from '@infrastructure/UuidV4Generator'


(async () => {
  console.log('Ejecutoo');
  const inMemoryUserRepo = InMemoryUserRepository.getInstance();

  //console.log('user vacio:', inMemoryUserRepo.userData);


  const userCreatorUseCase = new UserCreatorUseCase(inMemoryUserRepo)
  const userToCreate: User = {
    name: 'Luciana',
    age: 12,
    username: 'luciana24',
    id: '1',
  }

  await userCreatorUseCase.run(userToCreate)

  // Obteniendo usuarios
  const userGetterUseCase = new UserGetterUseCase(inMemoryUserRepo)
  //const userGetterUseCase = new UserGetterUseCase(dynamoDBUserRepo)
  const usersReturned = await userGetterUseCase.run()
  console.log(usersReturned)

  // Actualizar usuarios
  //const userUpdaterUseCase = new UserUpdaterUseCase(dynamoDBUserRepo)
  const userUpdaterUseCase = new UserUpdaterUseCase(inMemoryUserRepo)

  await userUpdaterUseCase.run({
    id: '1',
    username: 'luci'
  })
  const usersReturned2 = await userGetterUseCase.run()
  console.log(usersReturned2)

  // Eliminar un usuario
  //const userDeleterUseCase = new UserDeleterUseCase(dynamoDBUserRepo)
//  const userDeleterUseCase = new UserDeleterUseCase(inMemoryUserRepo)
//  await userDeleterUseCase.run('1')

//  const usersReturned3 = await userGetterUseCase.run()
//  console.log('Eliminado: ', usersReturned3)
  //console.log(inMemoryUserRepo.userData);

  /*
  dotenv.config({
    path: path.resolve(__dirname, '../../../../.env')
  })
  const dynamoDBUserRepo = new DynamoDBUserRepository()
  const uuidV4Generator = new UuidV4Generator()

  // Creando usuarios
  const userCreatorUseCase = new UserCreatorUseCase(dynamoDBUserRepo, uuidV4Generator)
  await userCreatorUseCase.run({
    name: 'Luciana',
    age: 12,
    username: 'luciana24',
  })



  const usersReturned2 = await userGetterUseCase.run()
  console.log(usersReturned2)

  */
})()
