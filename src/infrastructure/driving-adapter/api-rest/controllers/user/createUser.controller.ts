import { NextFunction, Request, Response } from 'express'
//import { DynamoDBUserRepository } from '@infrastructure/implementations/Aws/dynamo-db/DynamoDBUserRepository'
//import { UserCreatorUseCase } from '@application/usecases/UserCreator'
import { UserCreatorUseCase } from '../../../../../application/usecases/UserCreator'
//import { UuidV4Generator } from '@infrastructure/UuidV4Generator'
//import { InMemoryUserRepository } from '../../../../implementations/InMemory/InLowdbUserRepository'
import { InLowdbUserRepository } from '../../../../implementations/Lowdb/InLowdbUserRepository'

export const createUser = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
  const {
    username,
    age,
    name
  } = req.body

  const inMemoryUserRepo = InLowdbUserRepository.getInstance();
  const userCreatorUseCase = new UserCreatorUseCase(inMemoryUserRepo)
  //const dynamoDBUserRepo = new DynamoDBUserRepository()
  //const uuidV4Generator = new UuidV4Generator()
  //const userCreatorUseCase = new UserCreatorUseCase(dynamoDBUserRepo, uuidV4Generator)

  try {
    const userCreated = await userCreatorUseCase.run({
      id: '34234',
      name,
      username,
      age
    })

    res.json(userCreated)
    return
  } catch (e) {
    return next(e)
  }
}
