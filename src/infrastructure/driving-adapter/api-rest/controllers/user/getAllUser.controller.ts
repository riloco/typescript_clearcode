import { NextFunction, Request, Response } from 'express'
//import { DynamoDBUserRepository } from '../../../../implementations/Aws/dynamo-db/DynamoDBUserRepository'
import { UserGetterUseCase } from '../../../../../application/usecases/UserGetter'
import { InMemoryUserRepository } from '../../../../implementations/InMemory/InMemoryUserRepository'

export const getAllUsers = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
  //const dynamoDBUserRepo = new DynamoDBUserRepository()
  //const userGetterUseCase = new UserGetterUseCase(dynamoDBUserRepo)
  const inMemoryUserRepo = InMemoryUserRepository.getInstance();
  const userGetterUseCase = new UserGetterUseCase(inMemoryUserRepo)

  try {
    const users = await userGetterUseCase.run()
    res.json(users)
    return
  } catch (e) {
    return next(e)
  }
}
