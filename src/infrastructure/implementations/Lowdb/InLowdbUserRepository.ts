import { User } from '../../../domain/entities/user/User'
import { UserRepository } from '../../../domain/repositories/UserRepository'
import { getConnection } from '../../driven-adapter/database'

export class InLowdbUserRepository implements UserRepository {
  private static instance: InLowdbUserRepository;
  //private userData: User[] = []
  
  private constructor(){}

  public static getInstance(){
    if(!InLowdbUserRepository.instance){
      InLowdbUserRepository.instance = new InLowdbUserRepository();
      console.log('Nueva instancia')
    }
    return InLowdbUserRepository.instance;
  }

  async getAll (): Promise<User[]> {
    //console.log('Todos: ', this.userData);
    //return this.userData

    return [{id:"44", name: "name", username:"username", age: 5}]
  }

  async save (user: User): Promise<User> {
    //this.userData.push(user)
    //const db: any = getConnection();
    getConnection().data.users.push(user);
    await getConnection().write();
    return user
  }

  async getByUserName (username: string): Promise<User | null> {
    //const userFound = this.userData.find(x => x.username === username)

    //if (userFound === undefined) return null

    //return userFound
    return {id:"44", name: "name", username:"username", age: 5}
  }

  async update (user: User): Promise<User> {
    //const users = this.userData.filter(x => x.id !== user.id)
    //users.push(user)
    //this.userData = users
    //return user
    return {id:"44", name: "name", username:"username", age: 5}
  }

  async delete (user: User): Promise<void> {
    //const users = this.userData.filter(x => x.id !== user.id)
    //this.userData = users
  }

  async getById (id: string): Promise<User | null> {
    //const userFound = this.userData.find(x => x.id === id)

    //if (userFound === undefined) return null

    //return userFound
    return {id:"44", name: "name", username:"username", age: 5}
  }
}
