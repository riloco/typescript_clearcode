import { User } from '../../../domain/entities/user/User'
import { UserRepository } from '../../../domain/repositories/UserRepository'

export class InMemoryUserRepository implements UserRepository {
  private static instance: InMemoryUserRepository;
  private userData: User[] = []

  private constructor(){}

  public static getInstance(){
    if(!InMemoryUserRepository.instance){
      InMemoryUserRepository.instance = new InMemoryUserRepository();
      console.log('Nueva instancia')
    }
    return InMemoryUserRepository.instance;
  }

  async getAll (): Promise<User[]> {
    console.log('Todos: ', this.userData);
    return this.userData
//    return [{id:"44", name: "name", username:"username", age: 5}]
  }

  async save (user: User): Promise<User> {
    this.userData.push(user)
    return user
  }

  async getByUserName (username: string): Promise<User | null> {
    const userFound = this.userData.find(x => x.username === username)

    if (userFound === undefined) return null

    return userFound
  }

  async update (user: User): Promise<User> {
    const users = this.userData.filter(x => x.id !== user.id)
    users.push(user)
    this.userData = users
    return user
  }

  async delete (user: User): Promise<void> {
    const users = this.userData.filter(x => x.id !== user.id)
    this.userData = users
  }

  async getById (id: string): Promise<User | null> {
    const userFound = this.userData.find(x => x.id === id)

    if (userFound === undefined) return null

    return userFound
  }
}
