
[Video guia][https://www.youtube.com/watch?v=pRI04OE5QXM&list=PLAZUzPw7MqPSWbqXibVBfon4Y5HgQT9EU]

## Instalaciones
- npm init --yes
- npm install express dotenv uuid
- npm i -D typescript ts-node nodemosn eslint-config-standar-with-typescript @types/express @types/node @types/uuid
- tsc --init   :Crear archivo con configuracion de typescript

## Extenciones
- ESLint
- Error Lens

## Problemas encontrados

- Nodemon no reconocia archivos .ts: Solucion eliminar del package.json `types: modules` e instalar `npm install -g ts-node`

## servicios

- curl -X POST http://localhost:2426/users -d '{"username":"pepo", "name": "santiango", "age": "33"}' -H "Content-Type: application/json"
- curl -X GET http://localhost:2426/users
- curl -X DELETE http://localhost:3000/brands/1 -i 
- curl -X PUT http://localhost:3000/auth/login -d '{"email": "santiango@mail.com", "password": "Molina"}' -H "Content-Type: application/json"
